using BasicFramework.Mono;
using BasicFramework.Resource;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Audio
{
    public class AudioManager : BaseManager<AudioManager>
    {
        // 唯一的背景音乐组件
        private AudioSource bgMusic = null;
        // 背景音量大小
        private float bgMusicValue = 1;

        // 音效依附对象
        private GameObject audioPlayer = null;
        // 音效列表
        private List<AudioSource> audioSourceList = new List<AudioSource>();
        // 音效音量大小
        private float soundValue = 1;

        /// <summary>
        /// 播放背景音乐
        /// </summary>
        public void PlayBGMusic(string name)
        {
            if (bgMusic == null)
            {
                GameObject obj = new GameObject();
                obj.name = "BackgroundMusic";
                bgMusic = obj.AddComponent<AudioSource>();
            }

            ResourcesManager.Instance.LoadAsync<AudioClip>("Audio/BG/" + name, (clip) =>
            {
                bgMusic.clip = clip;
                bgMusic.loop = true;
                bgMusic.volume = bgMusicValue;
                bgMusic.Play();
            });
        }

        /// <summary>
        /// 改变背景音乐的音量大小
        /// </summary>
        /// <param name="value"></param>
        public void ChangeBGMusicValue(float value)
        {
            bgMusicValue = value;
            if (bgMusic == null)
                return;
            bgMusic.volume = bgMusicValue;
        }

        /// <summary>
        /// 暂停背景音乐
        /// </summary>
        public void PauseBGMusic()
        {
            if (bgMusic == null)
                return;
            bgMusic.Pause();
        }

        /// <summary>
        /// 停止背景音乐
        /// </summary>
        public void StopBGMusic()
        {
            if (bgMusic == null)
                return;
            bgMusic.Stop();
        }

        /// <summary>
        /// 播放音效
        /// </summary>
        /// <param name="name"></param>
        public void PlaySound(string name, bool isLoop = false, UnityAction<AudioSource> callback = null)
        {
            if (audioPlayer == null)
            {
                audioPlayer = new GameObject();
                audioPlayer.name = "AudioPlayer";
            }

            // 当音效资源加载结束后，再添加音效组件
            ResourcesManager.Instance.LoadAsync<AudioClip>("Audio/" + name, (clip) =>
            {
                AudioSource source = audioPlayer.AddComponent<AudioSource>();
                source.clip = clip;
                source.loop = isLoop;
                //source.volume = bgMusicValue;
                source.Play();
                audioSourceList.Add(source);
                if (callback != null)
                {
                    callback(source);
                }
            });
        }

        /// <summary>
        /// 调整声音大小
        /// </summary>
        /// <param name="value"></param>
        public void ChangeSoundVaule(float value)
        {
            soundValue = value;
            for (int i = 0; i < audioSourceList.Count; ++i)
            {
                audioSourceList[i].volume = value;
            }
        }

        /// <summary>
        /// 停止音效
        /// </summary>
        public void StopSound(AudioSource source)
        {
            if (audioSourceList.Contains(source))
            {
                audioSourceList.Remove(source);
                source.Stop();
                GameObject.Destroy(source);
            }
        }

        public AudioManager()
        {
            MonoManager.Instance.AddUpdateListener(AudioAutoDestroy);
        }

        /// <summary>
        /// 自动移除停止播放的组件
        /// </summary>
        private void AudioAutoDestroy()
        {
            for (int i = audioSourceList.Count - 1; i >= 0; --i)
            {
                if (!audioSourceList[i].isPlaying)
                {
                    GameObject.Destroy(audioSourceList[i]);
                    audioSourceList.RemoveAt(i);
                }
            }
        }
    }
}