using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Mono
{
    public class MonoController : MonoBehaviour
    {
        public event UnityAction updateEvent;

        private void Start()
        {
            DontDestroyOnLoad(this.gameObject);
        }

        private void Update()
        {
            if (updateEvent != null)
            {
                updateEvent();
            }
        }

        /// <summary>
        /// 给外部调用，添加帧更新事件的函数
        /// </summary>
        /// <param name="func"></param>
        public void AddUpdateListener(UnityAction func)
        {
            updateEvent += func;
        }

        /// <summary>
        /// 提供给外部，用于移除帧更新事件
        /// </summary>
        /// <param name="func"></param>
        public void RemoveUpdateListener(UnityAction func)
        {
            updateEvent -= func;
        }
    }
}