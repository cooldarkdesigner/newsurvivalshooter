using BasicFramework.Resource;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Pool
{
    /// <summary>
    /// �㼶����
    /// </summary>
    public class PoolData
    {
        public GameObject fatherObj;
        public List<GameObject> poolList;

        public PoolData(GameObject obj, GameObject poolObj)
        {
            fatherObj = new GameObject(obj.name);
            fatherObj.transform.parent = poolObj.transform;

            poolList = new List<GameObject>() { obj };

            PushObj(obj);
        }

        public void PushObj(GameObject obj)
        {
            obj.SetActive(false);
            poolList.Add(obj);
            obj.transform.parent = fatherObj.transform;
        }

        public GameObject GetObj()
        {
            GameObject obj = null;

            obj = poolList[0];
            poolList.RemoveAt(0);

            obj.SetActive(true);
            obj.transform.parent = null;
            return obj;
        }
    }

    /// <summary>
    /// �����ģ��
    /// </summary>
    public class PoolManager : BaseManager<PoolManager>
    {
        // ���������
        public Dictionary<string, PoolData> poolDic = new Dictionary<string, PoolData>();

        private GameObject poolObj;

        /// <summary>
        /// ��ȡ����
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public GameObject GetObj(string name, UnityAction<GameObject> callback)
        {
            GameObject obj = null;
            if (poolDic.ContainsKey(name) && poolDic[name].poolList.Count > 0)
            {
                //obj = poolDic[name].GetObj();
                callback(poolDic[name].GetObj());
            }
            else
            {
                //obj = GameObject.Instantiate(Resources.Load<GameObject>(name));
                //obj.name = name;
                ResourcesManager.Instance.LoadAsync<GameObject>(name, (obj) =>
                {
                    obj.name = name;
                });
            }

            return obj;
        }

        /// <summary>
        /// ���뻺���
        /// </summary>
        /// <param name="name"></param>
        /// <param name="obj"></param>
        public void PushObj(string name, GameObject obj)
        {
            if (poolObj == null)
            {
                poolObj = new GameObject("Pool");
            }

            if (poolDic.ContainsKey(name))
            {
                poolDic[name].PushObj(obj);
            }
            else
            {
                poolDic.Add(name, new PoolData(obj, poolObj));
            }
        }

        /// <summary>
        /// ��ջ����
        /// ���ڳ����л�
        /// </summary>
        public void Clean()
        {
            poolDic.Clear();
            poolObj = null;
        }
    }
}