using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Events;

namespace BasicFramework.Mono
{
    /// <summary>
    /// Mono管理
    /// 可以提供给外部添加帧更新事件的方法
    /// 可以提供外部协程的方法
    /// </summary>
    public class MonoManager : BaseManager<MonoManager>
    {
        private MonoController controller;

        public MonoManager()
        {
            GameObject obj = new GameObject("MonoController");
            controller = obj.AddComponent<MonoController>();
        }

        /// <summary>
        /// 给外部调用，添加帧更新事件的函数
        /// </summary>
        /// <param name="func"></param>
        public void AddUpdateListener(UnityAction func)
        {
            controller.AddUpdateListener(func);
        }

        /// <summary>
        /// 提供给外部，用于移除帧更新事件
        /// </summary>
        /// <param name="func"></param>
        public void RemoveUpdateListener(UnityAction func)
        {
            controller.RemoveUpdateListener(func);
        }

        public Coroutine StartCoroutine(IEnumerator routine)
        {
            return controller.StartCoroutine(routine);
        }

        public Coroutine StartCoroutine(string methodName, [DefaultValue("null")] object value)
        {
            return controller.StartCoroutine(methodName, value);
        }

        public Coroutine StartCoroutine(string methodName)
        {
            return controller.StartCoroutine(methodName);
        }
    }
}