using BasicFramework.Event;
using BasicFramework.Mono;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BasicFramework.Input
{
    public class InputManager : BaseManager<InputManager>
    {
        private bool isStart = false;

        private void CheckKeyCode(KeyCode key)
        {
            if (UnityEngine.Input.GetKeyDown(key))
            {
                EventCenter.Instance.EventTrigger("KeyPressDown", key);
            }
            if (UnityEngine.Input.GetKeyUp(key))
            {
                EventCenter.Instance.EventTrigger("KeyPressUp", key);
            }
        }

        public void StartOrEndCheck(bool isOpen)
        {
            isStart = isOpen;
        }

        public InputManager()
        {
            MonoManager.Instance.AddUpdateListener(InputUpdate);
        }

        private void InputUpdate()
        {
            if (!isStart)
                return;
            CheckKeyCode(KeyCode.W);
            CheckKeyCode(KeyCode.A);
            CheckKeyCode(KeyCode.S);
            CheckKeyCode(KeyCode.D);
        }
    }
}