using BasicFramework.Event;
using BasicFramework.Mono;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace BasicFramework.Scene 
{
    /// <summary>
    /// 场景切换管理
    /// </summary>
    public class ScenesManager : BaseManager<ScenesManager>
    {
        /// <summary>
        /// 同步切换场景
        /// </summary>
        /// <param name="name"></param>
        /// <param name="action"></param>
        public void LoadScene(string name, UnityAction action)
        {
            SceneManager.LoadScene(name);

            action.Invoke();
        }

        /// <summary>
        /// 提供给外部的 异步加载接口的方法
        /// </summary>
        /// <param name="name"></param>
        /// <param name="action"></param>
        public void LoadSceneAsync(string name, UnityAction action)
        {
            MonoManager.Instance.StartCoroutine(LoadSceneAsyncFunc(name, action));
        }

        /// <summary>
        /// 协程异步加载场景
        /// </summary>
        /// <param name="name"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        private IEnumerator LoadSceneAsyncFunc(string name, UnityAction action)
        {
            AsyncOperation ao = SceneManager.LoadSceneAsync(name);
            while (!ao.isDone)
            {
                EventCenter.Instance.EventTrigger("UpdateLoading", ao.progress);
                yield return ao.progress;
            }

            action.Invoke();
        }
    }
}