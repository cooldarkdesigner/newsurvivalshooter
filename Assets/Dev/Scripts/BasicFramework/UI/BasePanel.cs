using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BasicFramework.UI 
{
    /// <summary>
    /// 面板基类
    /// 找到所有自己面板下的控件对象
    /// 提供显示和隐藏的方法
    /// </summary>
    public class BasePanel : MonoBehaviour
    {
        private Dictionary<string, List<UIBehaviour>> controlDic = new Dictionary<string, List<UIBehaviour>>();

        protected virtual void Awake()
        {
            FindControlInChildren<Button>();
            FindControlInChildren<Image>();
            FindControlInChildren<Text>();
            FindControlInChildren<Toggle>();
            FindControlInChildren<Slider>();
            FindControlInChildren<ScrollRect>();
            FindControlInChildren<InputField>();
        }

        /// <summary>
        /// 得到对应名字的对应控件脚本
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="controlName"></param>
        /// <returns></returns>
        protected T GetControl<T>(string controlName) where T : UIBehaviour
        {
            if (controlDic.ContainsKey(controlName))
            {
                for (int i = 0; i < controlDic[controlName].Count; i++)
                {
                    if (controlDic[controlName][i] is T)
                    {
                        return controlDic[controlName][i] as T;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// 找到对应名字的对应控件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        private void FindControlInChildren<T>() where T : UIBehaviour
        {
            T[] controls = this.GetComponentsInChildren<T>();

            string objName;
            for (int i = 0; i < controls.Length; ++i)
            {
                objName = controls[i].gameObject.name;
                if (controlDic.ContainsKey(objName))
                {
                    controlDic[objName].Add(controls[i]);
                }
                else
                {
                    controlDic.Add(objName, new List<UIBehaviour>() { controls[i] });
                }

                if (controls[i] is Button)
                {
                    (controls[i] as Button).onClick.AddListener(() =>
                    {
                        OnClick(objName);
                    });
                }
                else if (controls[i] as Toggle)
                {
                    (controls[i] as Toggle).onValueChanged.AddListener((value) =>
                    {
                        OnValueChange(objName, value);
                    });
                }
            }
        }

        /// <summary>
        /// 按钮点击
        /// </summary>
        /// <param name="buttonName"></param>
        protected virtual void OnClick(string buttonName) { }

        /// <summary>
        /// 勾选
        /// </summary>
        /// <param name="toggleName"></param>
        protected virtual void OnValueChange(string toggleName, bool value) { }

        /// <summary>
        /// 显示
        /// </summary>
        public virtual void Show() { }

        /// <summary>
        /// 隐藏
        /// </summary>
        public virtual void Hide() { }
    }
}