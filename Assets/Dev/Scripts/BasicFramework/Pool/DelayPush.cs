using BasicFramework.Pool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayPush : MonoBehaviour
{
    private void OnEnable()
    {
        Invoke("Push", 1f);
    }

    private void Push()
    {
        PoolManager.Instance.PushObj(this.gameObject.name, this.gameObject);
    }
}
