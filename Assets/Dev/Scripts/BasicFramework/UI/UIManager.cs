using BasicFramework.Resource;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

namespace BasicFramework.UI
{
    /// <summary>
    /// UI层级
    /// </summary>
    public enum UILayer
    {
        Bottom,
        Middle,
        Top,
        System
    }

    /// <summary>
    /// UI管理
    /// 管理所有显示的面板
    /// 提供给外部显示和隐藏
    /// </summary>
    public class UIManager : BaseManager<UIManager>
    {
        public Dictionary<string, BasePanel> panelDic = new Dictionary<string, BasePanel>();

        private Transform bottom; // 底层
        private Transform middle; // 中层
        private Transform top; // 高层
        private Transform system; // 系统提示

        public RectTransform canvas;

        public UIManager()
        {
            GameObject tempObj = ResourcesManager.Instance.Load<GameObject>("UI/Canvas");
            canvas = tempObj.transform as RectTransform;
            GameObject.DontDestroyOnLoad(tempObj);

            bottom = canvas.Find("Bottom");
            middle = canvas.Find("Middle");
            top = canvas.Find("Top");
            system = canvas.Find("System");

            tempObj = ResourcesManager.Instance.Load<GameObject>("UI/EventSystem");
            Transform eventSystem = tempObj.transform;
            GameObject.DontDestroyOnLoad(tempObj);
        }

        /// <summary>
        /// 获取对应层级的父对象
        /// </summary>
        /// <param name="layer"></param>
        /// <returns></returns>
        public Transform GetLayerFather(UILayer layer)
        {
            switch (layer)
            {
                case UILayer.Bottom:
                    return this.bottom;
                case UILayer.Middle:
                    return this.middle;
                case UILayer.Top:
                    return this.top;
                case UILayer.System:
                    return this.system;
                default:
                    return null;
            }
        }

        /// <summary>
        /// 显示面板
        /// </summary>
        /// <typeparam name="T">面板脚本类型</typeparam>
        /// <param name="panelName">面板名</param>
        /// <param name="layer">面板所在层级</param>
        /// <param name="callBack">面板预设创建完成后，需要执行的时间</param>
        public void ShowPanel<T>(string panelName, UILayer layer = UILayer.Middle, UnityAction<T> callBack = null) where T : BasePanel
        {
            if (panelDic.ContainsKey(panelName))
            {
                panelDic[panelName].Show();

                if (callBack != null)
                {
                    callBack(panelDic[panelName] as T);
                }

                return;
            }

            ResourcesManager.Instance.LoadAsync<GameObject>($"UI/{panelName}", (obj) =>
            {
                Transform father = bottom;

                switch (layer)
                {
                    case UILayer.Middle:
                        father = middle;
                        break;
                    case UILayer.Top:
                        father = top;
                        break;
                    case UILayer.System:
                        father = system;
                        break;
                    default:
                        father = bottom;
                        break;
                }

                obj.transform.SetParent(father);
                obj.transform.localPosition = Vector3.zero;
                obj.transform.localScale = Vector3.one;

                (obj.transform as RectTransform).offsetMax = Vector2.zero;
                (obj.transform as RectTransform).offsetMin = Vector2.zero;

                T panel = obj.GetComponent<T>();

                if (callBack != null)
                {
                    callBack(panel);
                }

                panelDic.Add(panelName, panel);
            });
        }

        /// <summary>
        /// 隐藏面板
        /// </summary>
        /// <param name="panelName"></param>
        public void HidePanel(string panelName)
        {
            if (panelDic.ContainsKey(panelName))
            {
                GameObject.Destroy(panelDic[panelName].gameObject);
                panelDic.Remove(panelName);
            }
        }

        /// <summary>
        /// 得到指定显示的面板，方便外部使用
        /// </summary>
        public T GetPanel<T>(string panelName) where T : BasePanel
        {
            if (panelDic.ContainsKey(panelName))
            {
                return panelDic[panelName] as T;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 给控件添加自定义事件监听
        /// </summary>
        /// <param name="control">控件对象</param>
        /// <param name="type">事件类型</param>
        /// <param name="callBack">事件的响应函数</param>
        public static void AddCustomEventListener(UIBehaviour control, EventTriggerType type, UnityAction<BaseEventData> callBack)
        {
            EventTrigger trigger = control.GetComponent<EventTrigger>();

            if (trigger == null)
            {
                trigger = control.gameObject.AddComponent<EventTrigger>();
            }

            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = type;
            entry.callback.AddListener(callBack);

            trigger.triggers.Add(entry);
        }
    }
}